#include <stdio.h>
#include <stdlib.h>

int main(){
    double a, b, c, discriminant, x1,x2;
    printf("input coefficient of a: ");
    scanf("%lf", &a);
    printf("input coefficient of b: ");
    scanf("%lf", &b);
    printf("input coefficient of c: ");
    scanf("%lf", &c);
    discriminant=((b*b)-4*a*c);
    if(a!=0){
        if(discriminant>0){
            printf("it has distinct real roots\n");
            x1=(sqrt(discriminant)-b)/(2*a);
            x2=(-sqrt(discriminant)-b)/(2*a);
            printf("The roots of the equation are %f, %f\n",x1 ,x2);
        } else if(discriminant==0){
            printf("it has equal real roots\n");
            x1=(sqrt(discriminant)-b)/(2*a);
            printf("The root of the equation is %f\n",x1);
        } else{
            printf("it has complex roots that cant be calculated now\n");
        }
    } else{
        printf("ERROR Enter non-zero value");
    }
    return 0;
}
